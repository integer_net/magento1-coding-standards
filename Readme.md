# Magento 1 Coding Standards

## PHP Code

As a foundation, we use the official standard by the Marketplace Extension Quality Program (magento/marketplace-eqp):

The list is compiled based on the [MEQP1 4.0.0](https://github.com/magento/marketplace-eqp/blob/4.0.0/MEQP1/ruleset.xml) PHPCodeSniffer rule set. 

**We are changing this list with pull requests. Changes are merged after discussion in the PR and 2/3 majority of lead developers**

Rules with "avoid" or "should" are recommendations that do not apply in all cases.

### Formatting:
- If not overridden below, all [Zend Coding Standards](https://framework.zend.com/manual/1.12/en/coding-standard.html) from ZF1 apply
- Line length is limited to 120 characters
- Only one class or interface per file
- PHP closing tag `?>` only allowed in template files
- No UTF-8 BOM (Byte Order Mark) at the beginning of the file (can be configured in IDE/editor settings)
- Unix line breaks `\n`
- One space after comma `,`
- One space around assignment `=`
- [Allman (BSD)](https://en.wikipedia.org/wiki/Indent_style#Allman_style) indendation style for classes and functions/methods (not for loops and conditions, see Zend)
- SPACES, NOT TABS!!!
- All opening braces have to be on a new line, if they are not closed in the same line
- All closing braces that are not on the same line as the opening brace  must be on a new line and aligned with the line of the opening brace. E.g.:

        $output = $world->hello(
            strtoupper(
                trim('    hello world      ')
            )
        );

- There must be white space before all opening braces
- [PEAR standards for function calls](https://pear.php.net/manual/en/standards.funcalls.php)
- Function/method parameters with default values must be at the end of the declaration 

### General rules:
- Do not use old array syntax `array()`, use `[]` instead
- Direct output (`echo`, `printf`, content outside `<php ?>`) is only allowed in template files
- Forbidden functions: `error_log`, `print_r`, `var_dump`
- No call time pass-by-reference: `f(&$x);`
- No deprecated functions
- No `eval` and other functions that can execute input as PHP (`create_function`, `assert`, `preg_replace` with `e` modifier)
- Do not reuse the same counter variable in nested loops
- Do not use the "silence error" operator (`@`)
- Do not declare global functions
- No global variables (`global $x;` or `$GLOBALS['x']`)
- Do not use functions in loop conditions. E.g. instead of `for ($i = 1; $i < count($items); ++$i)` write `for ($i = 1, $count = count($items); $i < $count; ++$i)`
- Do not declare classes or methods as `final`
- Do not write `if` or `elseif` conditions that always evaluate to `true` or always to `false` (e.g. `if(1)`)
- No unused function/method parameters
- Do not override methods just to call their parent: `public function __construct($a, $b) { parent::__construct($a, $b); }`
- No "FIXME" or "TODO" comments
- No short open tags `<?` and `<?=`
- No empty `{ }` blocks in `catch`, loops and conditions
- No empty classes, interfaces, traits and functions
- Use `++` and `--` operators instead of `$x = $x + 1;` or `$x += 1`
- Do not comment out code. Either remove it or keep it.
- Avoid unreachable code like `return; $unreachable = true;`
- All properties and methods should have a visibility scope (`public`, `private` or `protected`)
- Variable names and attribute names, as well as keys in global arrays like `$_GET` should not contain numbers (e.g. instead of `$name1` and `$name2` use an array)
- Variable names and attribute names should be in `camelCase`
- Protected attribute and method names should start with an underscore
- Public and private attribute and method names should not start with an underscore
- Do not use `goto`
- Do not use the PHP4 attribute syntax `var $x`
- Do not use `+` for strings. The concatenation operator is `.`
- Do not use `==` and `!=` with the result of 'strpos()` or `stripos()`. Always check for identity with `===`  and '!==` instead

### Magento specific rules:
- Use `Mage::throwException()` instead of `throw new Exception()`
- Do not use deprecated `Mysql4` classes. Use `Resource` classes instead
- Do not instantiate blocks, helpers and models (or any core classes from the `Mage` namespace) directly with `new`. Use Magento factory methods instead.

    For library classes the restriction does not apply. This includes custom classes in the module that live in `app/code/*/*/Model` but are not real Magento models.

- Do not use `Zend_Db` methods like `join`, `where` or `query` outside of resource models
- Do not use `Exception` as class name in `throw` and `catch`, either use a FQN (`\Exception`), a different class name for the exception (like `MyModuleException`) or import the exception class differently (like `use MyModule\Exception as MyModuleException`)
- Avoid `$collection->count()` (implicitly loads collection) if you can use `$collection->getSize()` instead (makes a `SELECT COUNT(*)` query
- Do not use `count()` or `sizeof()` to check if an array is empty, use `empty()` instead
- Do not use `strlen()` to check if string is empty, use `!= ""` or `== ""` instead
- Avoid `$collection->getFirstItem()` if you only need one item. It does not limit the result of the load
- Avoid `$select->fetchAll()` for large data sets, because it loads everything at once
- Avoid `load()`, `save()` and `delete()` model methods in a loop, as well as methods that implicitly trigger a load
- Use MySQL indexes when creating new tables
- Avoid raw SQL statements, use the `Zend_DB` methods instead
- Avoid potentially slow SQL methods `GROUP BY`, `HAVING`, `DISTINCT`, `LIKE` and `UNION`
- Always implement the `_isAllowed()` method in admin controllers
- Avoid using any function in [this list](https://github.com/magento/marketplace-eqp/blob/1.0.5/MEQP1/Sniffs/Security/DiscouragedFunctionSniff.php#L35-L231)
- Do not use `include` or `require` with variable arguments or with URLs
- Do not use `exit`, `echo`, `print` or the backtick operator `\``. `echo` is allowed in template files.
- Do not use superglobals `$_GET`, `$_POST`, `$_SESSION`, `$_REQUEST`, `$_ENV` and `$GLOBALS`
- Avoid using superglobals `$_FILES`, `$_COOKIE` and `$_SERVER`
- Avoid date/time functions like `date()`, `time()`, `strftime()`, `date\_create`. Use `Mage::getSingleton('core/date')` for date/time operations instead. **If you decide not to, and use `DateTime`, you need to take care of timezones yourself!**
- Always escape the result of method calls in templates, except if the method ends with `Html`. Always return sanitized HTML in methods that end with `Html`
- For observer naming, refer to this page: [Observers](topics/observers.md)
- Layout XML must have a vendor prefix (RegEx for file name: `[a-z0-9]+_[a-z0-9]+\.xml`) and reside immediately in the layout directory (no subdirectory)
- Template directory must have a vendor prefix (RegEx for file name: `[a-z0-9]+_[a-z0-9]+\.xml`) and reside immediately in the template directory
- For helpers, refer to this page: [Helpers](topics/helpers.md)
- Internal module version numbers (in `config.xml`) start at `1.0.0` and are increased in the middle number when upgrade scripts are added: `1.1.0`, `1.2.0`, ...

    Only use patch version numbers (`1.1.1`) for multiple upgrade scripts in one feature / pull request, or to solve merge conflicts. Major version numbers (`2.0.0`) are not used.

    This is for internal version numbers of project specific modules, where semantic versioning is not needed. If a module is published, the release version numbers should follow semantic versioning, but they are not necessarily the same as the internal version numbers.

- Avoid using Varien_Object magic methods (`getXxx`, `setXxx`, `unsXxx`, `hasXxx`), use `getData`, `setData`, `unsetData`, `hasData` instead. An exception is when the magic methods are documented with phpdoc annotations, which the IDE understands, like this:

        /**
         * @method int getId()
         * @method setId(int $id)
         */
        class Foo_Bar extends Varien_Object
        {
        }

    For own models, using these annotations is the preferred method.

### Metrics:
- Max. cyclomatic complexity
- Max. nesting level


