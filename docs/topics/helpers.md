# Helpers

Core helpers are intended as public API of the module that expose services to other modules, which can also be used in the module itself.
We can do that too, but be careful to not create a mess of unrelated functions:

- Helper classes must be stateless (no mutable properties)
- Avoid private/protected methods. Any public method that's longer than a few lines should be extracted to a separate class. You may keep the helper to delegate to that new class if that *helps*, e.g. for easy access in templates.
- Keep the default "Data" helper as small as possible. Group methods in several helpers by responsibility, e.g. `Vendor_Module_Helper_Config` or `Vendor_Module_Helper_Factory`.
- A "Config" helper is a good place for methods that retrieve module configuration
