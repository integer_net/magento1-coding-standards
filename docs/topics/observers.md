# Observers

## Definition

Observers should be defined as `vendor_module` (RegEx: `[a-z0-9]+_[a-z0-9]+`). If that's ambiguous, add a suffix. Use the alias syntax to refer to the observer class:

```
    <observers>
        <vendor_module>
            <class>vendor_module/observer</class>
            <method>method</method>
        </vendor_module>
    </observers>
```

## Observer classes

- You may create a single class per module, as `Vendor_Module_Model_Observer` **OR** an observer class per area (`Vendor_Module_Model_Observer_{Adminhtml,Frontend,Global}`) **OR** an observer class per use case, or per event (`Vendor_Module_Observer_Xxx`)
- You may use the event name as method name (e.g. `catalogProductSaveBefore()` for the `catalog_product_save_before` event) **OR** use the method name to describe what the observer is doing. If you choose the latter, use the docblock comment to refer to the event(s):

        /**
         * @see event catalog_product_save_before
         */
        public function addProductMetaData(Varien_Event_Observer $observer)
        {
        }

- Keep observer methods as small as possible

    - Observer classes must not contain private/protected methods.
    - Observer methods should not contain more than 3-5 lines of code
    - Observer classes with multiple methods should only delegate to other classes
